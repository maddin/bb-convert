# BB Convert
Convert Mercurial repositories to Git.

## Workflow
1. Clone this repository to a private repository.
1. Set Environment variables in Pipeline settings of the repository
1. create a pipeline\
   This project already contains the necessary `bitbucket-piplines.yml`
1. on the next run the pipeline will generate a file `repo.list` in 
   the repository Downloads
1. copy `repo.list` to `repo.list.convert`
1. edit `repo.list.convert` if necessary
1. Check in `repo.list.convert` and push the changes
1. The pipeline trigger will convert all repositories in `repo.list.convert` 
   to an archived repo and convert the repo to a git repo

## Required configuration
The following environment variables are required:

| Variable Name | Description |
| ------------- | ----------- |
| BB_USERNAME   | Bitbucket User Name |
| BB_PASSWORD   | Bitbucket password | 

The specified user must be admin in order to convert the repositories.

