#!/usr/bin/env groovy

if (System.getenv('BB_USERNAME') == null || System.getenv('BB_PASSWORD') == null) {
    System.exit(1)
}

println "Scanning Repositories for user ${System.getenv('BB_USERNAME')}"

new BbApi(System.getenv('BB_USERNAME'), System.getenv('BB_PASSWORD')).with { bbApi ->

    def teams = []
    for (String nextPath = 'https://api.bitbucket.org/2.0/teams?role=admin&fields=-project,-owner,-description'; nextPath != null;) {
        def json = bbApi.getJson(nextPath)

        if (json.values != null) {
            teams.addAll(json.values)
        }
        nextPath = json.next
    }

    def userRepo = [username: System.getenv('BB_USERNAME')]
    teams.add(userRepo)

    def repos = [:]
    teams.each { team ->
        def teamRepos = []
        String nextPath
        if (team.links?.repositories?.href != null) {
            nextPath = team.links.repositories.href
        } else {
            nextPath = "https://api.bitbucket.org/2.0/users/${team.username}/repositories"
        }
        while (nextPath != null) {
            def json = bbApi.getJson(nextPath)

            json.values.each {
                def repoItem = [:]
                repoItem.scm = it.scm
                repoItem.url = it.links.self.href
                repoItem.name = it.full_name
                repoItem.project = it.project?.key
                repoItem.parent = it.parent?.full_name
                teamRepos.add(repoItem)
            }
            nextPath = json.next
        }
        repos.put(team.username, teamRepos)
    }

    def listFile = new File('repo.list')

    listFile.withPrintWriter('UTF-8') { outfile ->
        repos.each { k, v ->
            v.each {
                outfile.println "${it.name};${it.project ?: ''};${it.scm};${it.url};${it.parent ?: ''}"
            }
        }
    }

    listFile.eachLine {
        println it
    }
}

