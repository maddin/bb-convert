#!/usr/bin/env groovy
import groovy.json.JsonSlurper
import groovy.transform.TupleConstructor
@Grab('log4j:log4j:1.2.14')
import groovy.util.logging.Log4j
@Grab(group = 'org.apache.httpcomponents', module = 'httpclient', version = '4.5.12')
import org.apache.http.HttpHost
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.AuthCache
import org.apache.http.client.CredentialsProvider
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.auth.BasicScheme
import org.apache.http.impl.client.BasicAuthCache
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils


@Log4j
@TupleConstructor
class BbApi implements Closeable {
    final String bbUser
    final String bbPassword

    BBClientContext client;

    static void main(String[] args) {
        BbApi api = new BbApi(System.getenv('BB_USERNAME'), System.getenv('BB_PASSWORD'))
        println api.getJson('https://api.bitbucket.org/2.0/users/energycomponents/repositories')
//        api.get('https://api.bitbucket.org/2.0/users/energycomponents/repositories') { response ->
//            println(response.statusLine)
//            println(EntityUtils.toString(response.getEntity()))
//        }
    }

    BbApi(String username, String password) {
        bbUser = username
        bbPassword = password
    }

    BBClientContext client() {
        if (client == null) {
            synchronized(this) {
                if (client == null) {
                    HttpHost target = new HttpHost("api.bitbucket.org", 443, "https")
                    CredentialsProvider credsProvider = new BasicCredentialsProvider()
                    credsProvider.setCredentials(new AuthScope(target.getHostName(), target.getPort()), new UsernamePasswordCredentials(bbUser, bbPassword))
                    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build()

                    // Create AuthCache instance
                    AuthCache authCache = new BasicAuthCache()
                    // Generate BASIC scheme object and add it to the local
                    // auth cache
                    BasicScheme basicAuth = new BasicScheme()
                    authCache.put(target, basicAuth)

                    // Add AuthCache to the execution context
                    HttpClientContext localContext = HttpClientContext.create()
                    localContext.setAuthCache(authCache)

                    client = new BBClientContext(target, localContext, httpclient)
                }
            }
        }
        return client;
    }

    def get(String url, Closure responseClosure) {
        HttpGet get = new HttpGet(url);

        client().execute(get).with { response ->
            responseClosure(response)
        }
    }

    def getJson(String url) {
        return get(url) { response ->
            def json =new JsonSlurper().parseText(EntityUtils.toString(response.getEntity()))
            json
        }
    }

    @Override
    void close() throws IOException {
        if (client != null) {
            synchronized (this) {
                if (client != null) {
                    client.close()
                }
            }
        }
    }

    private static class BBClientContext implements Closeable {
        final HttpHost target
        final HttpClientContext context
        final CloseableHttpClient client

        private BBClientContext(HttpHost target, HttpClientContext context, CloseableHttpClient client) {
            this.target = target
            this.context = context
            this.client = client
        }

        CloseableHttpResponse execute(HttpGet get) {
            return client.execute(target, get, context)
        }

        @Override
        void close() throws IOException {
            client.close()

        }
    }
}
