#!/bin/bash -x

CONVERT_FILE=repo.list.convert

# HG authentication
cat > $HOME/.hgrc << EOF
[auth]
bb.prefix = https://bitbucket.org/
bb.username = $BB_USERNAME
bb.password = $BB_PASSWORD
EOF

# Git Authentication
cat > $HOME/.netrc <<EOF
machine bitbucket.org
  login $BB_USERNAME
  password $BB_PASSWORD
EOF

set -e

while IFS=';' read -r fullName project scm url parent
do
  if [ "$scm" = "hg" ] ; then
    IFS='/' read -r owner repoName <<<"$fullName"
    # rename repo
    curl -X PUT -u ${BB_USERNAME}:${BB_PASSWORD} \
      -H "Content-Type: application/json" \
      -d "{\"name\":\"${repoName}-archived\"}" \
      https://api.bitbucket.org/2.0/repositories/${fullName}

    cd /tmp
    hg clone https://bitbucket.org/${fullName}-archived
    mkdir ${repoName}
    cd ${repoName}
    git init
    /home/groovy/fast-export/hg-fast-export.sh -r ../${repoName}-archived

    # Create new repo
    echo -n '{"scm":"git","is_private": true ' > ../repo.data.${repoName}
    if [ -n "${project}" ] ; then
      echo -n ", \"project\": { \"key\": \"${project}\"}" >> ../repo.data.${repoName}
    fi
    echo '}' >> ../repo.data.${repoName}
    curl -X POST -u ${BB_USERNAME}:${BB_PASSWORD} \
      -H "Content-Type: application/json" \
      -d @../repo.data.${repoName} \
      https://api.bitbucket.org/2.0/repositories/${fullName}
    git remote add origin https://bitbucket.org/${fullName}
    git checkout HEAD
    git push -u origin master
  fi
done <$CONVERT_FILE